const fs = require('fs');
const qs = require('querystring');
const http = require('http');
const https = require('https');

let port = 3000;
let myForm = fs.readFileSync('form.html');

function translateFromEnglish(text, callback) {
  return https.get({
    host: 'translate.yandex.net',
    path: '/api/v1.5/tr.json/translate?key=trnsl.1.1.20160723T183155Z.f2a3339517e26a3c.d86d2dc91f2e374351379bb3fe371985273278df&lang=en-ru&text=' + escape(text)
  }, function(response) {
    let data = '';
    response.on('data', function(chunk) {
      data += chunk;
    });
    response.on('end', function() {
      callback(data);
    });
  })
};

function handler(request, response) {
  if (request.method == 'POST') {
    let data = '';
    request.on('data', function(chunk) {
      data += chunk;
    });
    request.on('end', function(chunk) {
      let postData = qs.parse(data);
      translateFromEnglish(postData.englishText, function(answer) {
        response.writeHead(200, 'OK', {'Content-Type': 'text/plain; charset=UTF-8'});
        let translateData = JSON.parse(answer);
        response.write(`Your text in Russian is: ${translateData.text[0]}`);
        response.end();
      });
    });
  }
  else {
    response.writeHead(200, 'OK', {'Content-Type': 'text/html'});
    response.write(`${myForm}`);
    response.end();
  }
};

const server = http.createServer();
server.on('error', function(error) { console.error(error) });
server.on('request', handler);
server.listen(port);
