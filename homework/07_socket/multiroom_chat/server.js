const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static('public'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(client) {
  client.on('clientConnectChat', function(data) {
    client.broadcast.emit('serverChatBroadcast', { chat: data.chat, message: data.who + " connected the chat"});
  });

  client.on('clientDisconnectChat', function(data) {
    client.broadcast.emit('serverChatBroadcast', { chat: data.chat, message: data.who + " disconnected the chat"});
  });

  client.on('clientChatMessage', function(data) {
    client.emit('serverChatBroadcast', { chat: data.chat, message: data.message });
    client.broadcast.emit('serverChatBroadcast', { chat: data.chat, message: data.message });
  });
});

http.listen(3000);
