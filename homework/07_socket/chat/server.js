const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static('public'));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(client) {
  client.on('clientChatJoin', function(userName) {
    client.broadcast.emit('serverChatBroadcast', userName + " joined the chat");
  });

  client.on('clientChatMessage', function(data) {
    client.emit('serverChatBroadcast', data);
    client.broadcast.emit('serverChatBroadcast', data);
  });
});

http.listen(3000);
