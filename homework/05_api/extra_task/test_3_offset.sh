#!/usr/bin/env bash

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {}, "id": 1}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {"offset": 2}, "id": 2}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {"limit": 1, "offset": 1}, "id": 3}' -H "Content-Type: application/json"
echo -e "\n"
