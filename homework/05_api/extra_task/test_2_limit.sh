#!/usr/bin/env bash

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {}, "id": 1}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {"limit": 0}, "id": 2}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {"limit": 2}, "id": 3}' -H "Content-Type: application/json"
echo -e "\n"
