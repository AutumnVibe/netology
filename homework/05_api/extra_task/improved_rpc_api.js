const express = require('express');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const app = express();

const rpcAPI = express.Router();

let usersCollection = [
  { id: 1, name: "Lola", score: 99 },
  { id: 2, name: "John", score: 9 },
  { id: 3, name: "Alex", score: 12 },
  { id: 4, name: "Irina", score: 100 },
  { id: 5, name: "Natalia", score: 1 }
];

let idCounter = usersCollection[usersCollection.length - 1]["id"];

function getNewId() {
  idCounter += 1;
  return idCounter;
}

function isValidScore(scoreToCheck) {
  if (!Number.isInteger(scoreToCheck)) {
    return false;
  }
  if (scoreToCheck < 0) {
    return false;
  }
  if (scoreToCheck > 100) {
    return false;
  }
  return true;
}

function isValidUserId(idToCheckRaw) {
  let idToCheck = parseInt(idToCheckRaw);
  if (!Number.isInteger(idToCheck)) {
    return false;
  }
  if (idToCheck < 1) {
    return false;
  }
  return true;
}

function isValidOffset(idToCheckRaw) {
  let idToCheck = parseInt(idToCheckRaw);
  if (!Number.isInteger(idToCheck)) {
    return false;
  }
  if (idToCheck < 1) {
    return false;
  }
  return true;
}

function isValidLimit(idToCheckRaw) {
  let idToCheck = parseInt(idToCheckRaw);
  if (!Number.isInteger(idToCheck)) {
    return false;
  }
  if (idToCheck < 0) {
    return false;
  }
  return true;
}

function checkUserInfo(contentToCheck) {
  if (!('name' in contentToCheck)) {
    return { passed: 0, info: "name field required" };
  }
  if (!('score' in contentToCheck)) {
    return { passed: 0, info: "score field required" };
  }
  if (!isValidScore(contentToCheck.score)) {
    return { passed: 0, info: "score must be an integer from 0 to 100" };
  }
  return { passed: 1 };
}

let showUsers = (params, callback) => {
  let offset = 0;
  var fields = { "name": 1, "id": 1, "score": 1 };
  if ('fields' in params) {
    fields = params.fields;
  }

  if ('offset' in params) {
    if (isValidLimit(params.offset)) {
      offset = parseInt(params.offset);
    }
    else {
      callback({ error: { code: 400, message: "Offset must be an integer above zero or zero" } });
    }
  }
  if ('id' in params) {
    const idToFindRaw = params.id;
    if (!isValidUserId(idToFindRaw)) {
      callback({ error: { code: 400, message: "Id must be an integer above zero" } });
    } else {
      const idToFind = parseInt(idToFindRaw);
      let needIndex = -1;
      for (let i = 0; i < usersCollection.length; i += 1) {
        if (idToFind == usersCollection[i].id) {
          needIndex = i;
        }
      }
      if (needIndex > -1) {
        callback(null, { result: { user: usersCollection[needIndex] } });
      }
      else {
        callback({ error: { code: 404, message: "Your id not found" } });
      }
    }
  }
  else {
    if ('limit' in params) {
      if (isValidLimit(params.limit)) {
        let tempUsers = [];
        let limit = parseInt(params.limit);
        for (let i = offset; i < usersCollection.length; i += 1) {
          if (i < limit + offset) {
            let tmpItem = {};
            for (let key in fields) {
              tmpItem[key] = usersCollection[i][key];
            }
            tempUsers.push(tmpItem);
          }
        }
        callback(null, { result: { users: tempUsers } });
      }
      else {
        callback({ error: { code: 400, message: "Limit must be an integer above zero or zero" } });
      }
    }
    else {
      
      let tempUsers = [];
      for (let i = offset; i < usersCollection.length; i += 1) {  
        let tmpItem = {};
        for (let key in fields) {
          tmpItem[key] = usersCollection[i][key];
        }
        tempUsers.push(tmpItem);
      }
      callback(null, { result: { users: tempUsers } });
    }
  }
};

let createUser = (params, callback) => {
  checkOfData = checkUserInfo(params);
  if (checkOfData.passed) {
    let newName = String(params.name);
    let newScore = params.score;
    let newId = getNewId();
    usersCollection.push({ id: newId, name: newName, score: newScore });
    callback(null, { result: { message: "New user created" } });
  }
  else {
    callback({ error: { code: 400, message: checkOfData.info } });
  }
};

let deleteUser = (params, callback) => {
  if ('id' in params) {
    const idToFindRaw = params.id;
    if (!isValidUserId(idToFindRaw)) {
      callback({ error: { code: 400, message: "Requiested id must be an integer above zero" } });
    } else {
      const idToFind = parseInt(idToFindRaw);
      let needIndex = -1;
      let newUsersCollection = [];
      for (let i = 0; i < usersCollection.length; i += 1) {
        if (idToFind == usersCollection[i].id) {
          needIndex = i;
        }
        else {
          newUsersCollection.push(usersCollection[i]);
        }
      }
      if (needIndex > -1) {
        usersCollection = newUsersCollection;
        callback(null, { result: { message: "Delete success" } });
      }
      else {
        callback({ error: { code: 404, message: "Your id not found" } });
      }
    }
  }
  else {
    callback({ error: { code: 404, message: "Your request must have an id" } });
  }
};

let wipeUsers = (params, callback) => {
  usersCollection = [];
  callback(null, { result: { message: "Wipe success" } });     
};

let updateUser = (params, callback) => {
  if ('id' in params) {
    const idToFindRaw = params.id;
    if (!isValidUserId(idToFindRaw)) {
      callback({ error: { code: 400, message: "Requiested id must be an integer above zero" } });
    } else {
      const idToFind = parseInt(idToFindRaw);
      let needIndex = -1;
      let newUsersCollection = [];
      for (let i = 0; i < usersCollection.length; i += 1) {
        if (idToFind == usersCollection[i].id) {
          needIndex = i;
        }
        else {
          newUsersCollection.push(usersCollection[i]);
        }
      }
      if (needIndex > -1) {
        checkOfData = checkUserInfo(params);
        if (checkOfData.passed) {
          let newName = String(params.name);
          let newScore = params.score;
          let newId = getNewId();
          newUsersCollection.push({ id: idToFind, name: newName, score: newScore });
          usersCollection = newUsersCollection;
          callback(null, { result: { message: "Update success" } });
        }
        else {
          callback({ error: { code: 400, message: checkOfData.info } });
        }
      }
      else {
        callback({ error: { code: 404, message: "Your id not found" } });
      }
    }
  }
  else {
    callback({ error: { code: 404, message: "Your request must have an id" } });
  }
};

// Improved CRUD RPC
let RPC = {
  "create": createUser,
  "read": showUsers,
  "update": updateUser,
  "delete": deleteUser,
  "wipe": wipeUsers,
};

rpcAPI.post("/rpc", jsonParser, function(req, res) {
  const method = RPC[req.body.method];
  method(req.body.params, function(error, result) {
    if (error) {
      result = error;
    }
    result["jsonrpc"] = "2.0";
    if ('id' in req.body) {
      result['id'] = req.body.id;
    }
    res.status(200).json(result);
  });
});


app.use("/", rpcAPI);

app.listen(3000, function() {});
