#!/usr/bin/env bash

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {}, "id": 1}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "update", "params": {"id": 100}, "id": 2}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "update", "params": {"id": -100}, "id": 3}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "update", "params": {"id": 3, "name": "William"}, "id": 4}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "update", "params": {"id": 3, "score": 22}, "id": 5}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "update", "params": {"id": 3, "name": "William", "score": 122}, "id": 6}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "update", "params": {"id": 3, "name": "William", "score": 22}, "id": 7}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {}, "id": 8}' -H "Content-Type: application/json"
echo -e "\n"
