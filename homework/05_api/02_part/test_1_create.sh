#!/usr/bin/env bash

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {}, "id": 1}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "create", "params": {"name": "Jim"}, "id": 2}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "create", "params": {"score": 33}, "id": 3}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "create", "params": {"name": "Jim", "score": 133}, "id": 4}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "create", "params": {"name": "Jim", "score": 33}, "id": 5}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/rpc -X POST -d '{"jsonrpc": "2.0", "method": "read", "params": {}, "id": 6}' -H "Content-Type: application/json"
echo -e "\n"
