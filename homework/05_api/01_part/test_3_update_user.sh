#!/usr/bin/env bash

curl localhost:3000/api/v1/users -X GET
echo -e "\n"

curl localhost:3000/api/v1/users/100 -X PUT -d '{"name": "Sonya", "score": 77}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users/1 -X PUT -d '{"name": "Sonya"}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users/1 -X PUT -d '{"score": 77}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users/1 -X PUT -d '{"name": "Sonya", "score": 177}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users/1 -X PUT -d '{"name": "Sonya", "score": 77}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users -X GET
echo -e "\n"
