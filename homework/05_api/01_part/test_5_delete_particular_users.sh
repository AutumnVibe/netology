#!/usr/bin/env bash


curl localhost:3000/api/v1/users -X GET
echo -e "\n"

curl localhost:3000/api/v1/users/100 -X DELETE
echo -e "\n"

curl localhost:3000/api/v1/users/1 -X DELETE
echo -e "\n"

curl localhost:3000/api/v1/users/2 -X DELETE
echo -e "\n"

curl localhost:3000/api/v1/users -X GET
echo -e "\n"
