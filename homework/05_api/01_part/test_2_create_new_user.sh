#!/usr/bin/env bash

curl localhost:3000/api/v1/users -X GET
echo -e "\n"

curl localhost:3000/api/v1/users -X POST -d '{"score": 50}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users -X POST -d '{"name": "Daria"}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users -X POST -d '{"name": "Daria", "score": 200}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users -X POST -d '{"name": "Daria", "score": 50}' -H "Content-Type: application/json"
echo -e "\n"

curl localhost:3000/api/v1/users -X GET
echo -e "\n"
