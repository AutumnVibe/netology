const express = require('express');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const app = express();

const rtAPIv1 = express.Router();

let usersCollection = [
  { id: 1, name: "Lola", score: 99 },
  { id: 2, name: "John", score: 9 },
  { id: 3, name: "Alex", score: 12 },
  { id: 4, name: "Irina", score: 100 },
  { id: 5, name: "Natalia", score: 1 }
];

let idCounter = usersCollection[usersCollection.length - 1]["id"];

function getNewId() {
  idCounter += 1;
  return idCounter;
}

function isValidScore(scoreToCheck) {
  if (!Number.isInteger(scoreToCheck)) {
    return false;
  }
  if (scoreToCheck < 0) {
    return false;
  }
  if (scoreToCheck > 100) {
    return false;
  }
  return true;
}

function isValidUserId(idToCheckRaw) {
  let idToCheck = parseInt(idToCheckRaw);
  if (!Number.isInteger(idToCheck)) {
    return false;
  }
  if (idToCheck < 1) {
    return false;
  }
  return true;
}

function checkUserInfo(contentToCheck) {
  if (!('name' in contentToCheck)) {
    return { passed: 0, info: "name field required" };
  }
  if (!('score' in contentToCheck)) {
    return { passed: 0, info: "score field required" };
  }
  if (!isValidScore(contentToCheck.score)) {
    return { passed: 0, info: "score must be an integer from 0 to 100" };
  }
  return { passed: 1 };
}

rtAPIv1.get("/users/", function(req, res) {
  res.status(200).json({users: usersCollection});
});

rtAPIv1.post("/users", jsonParser, function(req, res) {
  checkOfData = checkUserInfo(req.body);
  if (checkOfData.passed) {
    let newName = String(req.body.name);
    let newScore = req.body.score;
    let newId = getNewId();
    usersCollection.push({ id: newId, name: newName, score: newScore });
    res.status(200).json({info: "new user created"});
  }
  else {
    res.status(400).json({info: checkOfData.info});
  }
});

rtAPIv1.get("/users/:id", function(req, res) {
  const idToFindRaw = req.params.id;
  if (!isValidUserId(idToFindRaw)) {
    res.status(400).json({info: "id must be an integer above zero"});
  } else {
    const idToFind = parseInt(idToFindRaw);
    let needIndex = -1;
    for (let i = 0; i < usersCollection.length; i += 1) {
      if (idToFind == usersCollection[i].id) {
        needIndex = i;
      }
    }
    if (needIndex > -1) {
      res.status(200).json({user: usersCollection[needIndex]});
    }
    else {
      res.status(404).json({info: "your id not found"});
    }
  }
});

rtAPIv1.put("/users/:id", jsonParser, function(req, res) {
  const idToFindRaw = req.params.id;
  if (!isValidUserId(idToFindRaw)) {
    res.status(400).json({info: "id must be an integer above zero"});
  } else {
    checkOfData = checkUserInfo(req.body);
    if (!checkOfData.passed) {
      res.status(400).json({info: checkOfData.info});
    }
    else {
      let newName = String(req.body.name);
      let newScore = req.body.score;
      const idToFind = parseInt(idToFindRaw);
      let needIndex = -1;
      let newUsersCollection = [];
      for (let i = 0; i < usersCollection.length; i += 1) {
        if (idToFind == usersCollection[i].id) {
          needIndex = i;
        }
        else {
          newUsersCollection.push(usersCollection[i]);
        }
      }
      if (needIndex > -1) {
        newUsersCollection.push({ id: idToFind, name: newName, score: newScore });
        usersCollection = newUsersCollection;
        res.status(200).json({info: "update success"});
      }
      else {
        res.status(404).json({info: "your id not found"});
      }
    }
  }
});

rtAPIv1.delete("/users/:id", function(req, res) {
  const idToFindRaw = req.params.id;
  if (!isValidUserId(idToFindRaw)) {
    res.status(400).json({info: "id must be an integer above zero"});
  } else {
    const idToFind = parseInt(idToFindRaw);
    let needIndex = -1;
    let newUsersCollection = [];
    for (let i = 0; i < usersCollection.length; i += 1) {
      if (idToFind == usersCollection[i].id) {
        needIndex = i;
      }
      else {
        newUsersCollection.push(usersCollection[i]);
      }
    }
    if (needIndex > -1) {
      usersCollection = newUsersCollection;
      res.status(200).json({info: "delete success"});
    }
    else {
      res.status(404).json({info: "your id not found"});
    }
  }
});

app.use("/api/v1", rtAPIv1);

app.listen(3000, function() {});
