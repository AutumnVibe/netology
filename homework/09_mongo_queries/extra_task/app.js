const mongodb = require('mongodb');
const express = require('express');
const bodyParser = require('body-parser');
const nunjucks = require( 'nunjucks' ) ;

const urlencodedParser = bodyParser.urlencoded({extended: true});

const mongoClient = mongodb.MongoClient;
const mongoUrl = 'mongodb://localhost:27017/test_database';

const app = express();

const router = express.Router();

const pathToTemplates = 'templates' ;
nunjucks.configure(pathToTemplates, {
    autoescape: true,
    express: app
} ) ;

let contactsCollection = [
  { contactId: 1, name: "Lola", surname: "Malkovich", phone: "+7 (999) 111-12-33", additionalInfo: "email: mail@example.com" },
  { contactId: 2, name: "John", surname: "Petrov", phone: "+1 (555) 555-55-55", additionalInfo: "skype: petrov777" },
  { contactId: 3, name: "Alex", surname: "Filton", phone: "+7 (495) 111-00-00" },
  { contactId: 4, name: "Irina", surname: "Vronskaya", phone: "+7 (923) 987-98-98" },
  { contactId: 5, name: "Natalia", surname: "Kvasova", phone: "+1 (111) 123-11-22" },
  { contactId: 6, name: "Nastya", surname: "Alekseeva", phone: "+7 (925) 222-11-22" },
  { contactId: 7, name: "Marina", surname: "Shtein", phone: "+1 (555) 123-77-88" },
  { contactId: 8, name: "Alena", surname: "Shtein", phone: "+1 (555) 123-99-99" },
  { contactId: 9, name: "Natalia", surname: "Ivanova", phone: "+7 (913) 333-11-11" }
];

let idCounter = contactsCollection[contactsCollection.length - 1]["contactId"];

function getNewId() {
  idCounter += 1;
  return idCounter;
}

function isValidUserId(idToCheckRaw) {
  let idToCheck = parseInt(idToCheckRaw);
  if (!Number.isInteger(idToCheck)) {
    return false;
  }
  if (idToCheck < 1) {
    return false;
  }
  return true;
}

function checkUserInfo(contentToCheck) {
  if (!('name' in contentToCheck)) {
    return { passed: 0, info: "name field required" };
  }
  if (!('surname' in contentToCheck)) {
    return { passed: 0, info: "surname field required" };
  }
  if (!('phone' in contentToCheck)) {
    return { passed: 0, info: "phone field required" };
  }
  return { passed: 1 };
}

function addFirstUsers() {
  mongoClient.connect(mongoUrl, (err, db) => {
    if (err) {
      console.log("Can't connect to the MongoDB. Error: ", err);
    }
    else {
      let collection = db.collection('contacts');

      collection.insert(contactsCollection, (err) => {
        if (err) {
          console.log("Can't insert first contacts. Error: ", err);
        }
      });
      db.close();
    }
  });
};

let readContacts = searchForThis => {
  return new Promise((done, fail) => {
    mongoClient.connect(mongoUrl)
      .then((db) => {
        let collection = db.collection('contacts');
        collection.find(searchForThis, { _id: false }).toArray((err, result) => {
          if (err) {
            fail({ errorText: "Can't show entries. Error: " + err });
          }
          else if (result.length) {
            done({ contacts: result });
          }
          else {
            fail({ errorText: "Users not found" });
          }
        });
        db.close();
      })
      .catch((connectionError) => {
        if (connectionError) {
          fail({ errorText: "Can't connect to the MongoDB. Error: " + connectionError });
        }
      });
  });
};

let addContact = possibleContact => {
  return new Promise((done, fail) => {
    mongoClient.connect(mongoUrl)
      .then((db) => {
        let collection = db.collection('contacts');
        collection.insert([ possibleContact ], (err) => {
          if (err) {
            fail({ errorText: "Can't add entry. Error: " + err });
          }
          else {
            done({ status: "Contact added" });
          }
        });
        db.close();
      })
      .catch((connectionError) => {
        if (connectionError) {
          fail({ errorText: "Can't connect to the MongoDB. Error: " + connectionError });
        }
      });
  });
};

let deleteContact = contactIdToRemove => {
  return new Promise((done, fail) => {
    mongoClient.connect(mongoUrl)
      .then((db) => {
        let collection = db.collection('contacts');
        collection.deleteOne({ contactId: contactIdToRemove }, (err, result) => {
          if (err) {

            fail({ errorText: "Can't update contacts. Error: " + err });
          }
          else if (!result.deletedCount) {
            fail({ errorText: "Nothing to delete: requested id not found"});
          }
          else {
            done({ status: "Remove success" });
          }
        });
        db.close();
      })
      .catch((connectionError) => {
        if (connectionError) {
          fail({ errorText: "Can't connect to the MongoDB. Error: " + connectionError });
        }
      });
  });
};


// Main page
router.get("/", function (req, res) {
  readContacts({})
    .then(contactsFound => { res.render( 'index.html', { contactsAll: contactsFound } ); })
    .catch(errorMessage => { res.status(400).json(errorMessage); })
});

// One contact page
router.get("/contact/:id", function (req, res) {
  const idToFindRaw = req.params.id;
  if (!isValidUserId(idToFindRaw)) {
    res.status(400).json({info: "id must be an integer above zero"});
  }
  else {
    const idToFind = parseInt(idToFindRaw);
    let searchForThis = {"contactId": idToFind};

    readContacts(searchForThis)
      .then(contactsFound => { res.render( 'one_contact.html', { contactOne: contactsFound } )})
      .catch(errorMessage => { res.status(400).json(errorMessage); })
  }
});

// Remove contact
router.get("/delete/:id", function(req, res) {
  const idToFindRaw = req.params.id;
  if (!isValidUserId(idToFindRaw)) {
    res.status(400).json({info: "id must be an integer above zero"});
  } else {
    const idToFind = parseInt(idToFindRaw);
    deleteContact(idToFind)
      .then(statusMessage => { res.redirect('/delete_success'); })
      .catch(() => { res.status(404).json({info: "your id not found"}); });
  }
});

router.get("/delete_success", function(req, res) {
  res.render('delete_success.html');
});

router.get("/add_contact", function(req, res) {
  res.render('add_contact.html');
});

// New contact
router.post("/add_contact", urlencodedParser, function(req, res) {
  checkOfData = checkUserInfo(req.body);
  if (checkOfData.passed) {
    let newName = String(req.body.name);
    let newSurname = String(req.body.surname);
    let newPhone = String(req.body.phone);
    let newAdditionalInfo = String(req.body.additionalInfo);

    let supportInfo = {};
    let needFix = 0;
    let fillerInfo = { name: newName, surname: newSurname, phone: newPhone };

    if (newName === '') {
      supportInfo['name'] = 'Name required';
      needFix = 1;
    }
    if (newSurname === '') {
      supportInfo['surname'] = 'Surname required';
      needFix = 1;
    }
    if (newPhone === '') {
      supportInfo['phone'] = 'Phone required';
      needFix = 1;
    }
    if (needFix) {
      res.render('add_contact.html', { supportInfo: supportInfo, fillerInfo: fillerInfo });
    }
    else {
      let newId = getNewId();
      addContact({ name: newName, surname: newSurname, contactId: newId, phone: newPhone, additionalInfo: newAdditionalInfo })
        .then(statusMessage => { res.redirect('/create_success'); })
        .catch( errorMessage => { res.status(400).json(errorMessage); });
    }
  }
  else {
    res.status(400).json({info: checkOfData.info});
  }
});

router.get("/create_success", function(req, res) {
  res.render('create_success.html');
});

addFirstUsers();

app.use("/", router);
app.use(express.static('public'));

app.listen(3000, function () { });
