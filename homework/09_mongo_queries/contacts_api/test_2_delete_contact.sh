#!/usr/bin/env bash

curl http://localhost:3000/api/v1/contacts -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/1 -i -X DELETE
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/2 -i -X DELETE
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/3 -i -X DELETE
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/7777 -i -X DELETE
echo -e "\n";

curl http://localhost:3000/api/v1/contacts -i -X GET
echo -e "\n";
