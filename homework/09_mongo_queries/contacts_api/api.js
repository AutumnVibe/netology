const mongodb = require('mongodb');
const express = require('express');
const bodyParser = require('body-parser');

const jsonParser = bodyParser.json();

const mongoClient = mongodb.MongoClient;
const mongoUrl = 'mongodb://localhost:27017/test_database';

const app = express();

const rtAPIv1 = express.Router();

let contactsCollection = [
  { contactId: 1, name: "Lola", surname: "Malkovich", phone: "+7 (999) 111-12-33" },
  { contactId: 2, name: "John", surname: "Petrov", phone: "+1 (555) 555-55-55" },
  { contactId: 3, name: "Alex", surname: "Filton", phone: "+7 (495) 111-00-00" },
  { contactId: 4, name: "Irina", surname: "Vronskaya", phone: "+7 (923) 987-98-98" },
  { contactId: 5, name: "Natalia", surname: "Kvasova", phone: "+1 (111) 123-11-22" },
  { contactId: 6, name: "Nastya", surname: "Alekseeva", phone: "+7 (925) 222-11-22" },
  { contactId: 7, name: "Marina", surname: "Shtein", phone: "+1 (555) 123-77-88" },
  { contactId: 8, name: "Alena", surname: "Shtein", phone: "+1 (555) 123-99-99" },
  { contactId: 9, name: "Natalia", surname: "Ivanova", phone: "+7 (913) 333-11-11" }
];

let idCounter = contactsCollection[contactsCollection.length - 1]["contactId"];

function getNewId() {
  idCounter += 1;
  return idCounter;
}

function isValidUserId(idToCheckRaw) {
  let idToCheck = parseInt(idToCheckRaw);
  if (!Number.isInteger(idToCheck)) {
    return false;
  }
  if (idToCheck < 1) {
    return false;
  }
  return true;
}

function checkUserInfo(contentToCheck) {
  if (!('name' in contentToCheck)) {
    return { passed: 0, info: "name field required" };
  }
  if (!('surname' in contentToCheck)) {
    return { passed: 0, info: "surname field required" };
  }
  if (!('phone' in contentToCheck)) {
    return { passed: 0, info: "phone field required" };
  }
  return { passed: 1 };
}

function addFirstUsers() {
  mongoClient.connect(mongoUrl, (err, db) => {
    if (err) {
      console.log("Can't connect to the MongoDB. Error: ", err);
    }
    else {
      let collection = db.collection('contacts');

      collection.insert(contactsCollection, (err) => {
        if (err) {
          console.log("Can't insert first contacts. Error: ", err);
        }
      });
      db.close();
    }
  });
};

let readContacts = searchForThis => {
  return new Promise((done, fail) => {
    mongoClient.connect(mongoUrl)
      .then((db) => {
        let collection = db.collection('contacts');
        collection.find(searchForThis, { _id: false }).toArray((err, result) => {
          if (err) {
            fail({ errorText: "Can't show entries. Error: " + err });
          }
          else if (result.length) {
            done({ contacts: result });
          }
          else {
            fail({ errorText: "Users not found" });
          }
        });
        db.close();
      })
      .catch((connectionError) => {
        if (connectionError) {
          fail({ errorText: "Can't connect to the MongoDB. Error: " + connectionError });
        }
      });
  });
};

let updateContact = (idToUpdate, newValues) => {
  return new Promise((done, fail) => {
    mongoClient.connect(mongoUrl)
      .then((db) => {
        let collection = db.collection('contacts');
        collection.updateOne({"contactId": idToUpdate}, { "$set": { "name": newValues.name, "surname": newValues.surname, "phone": newValues.phone } }, (err, result) => {
          if (err) {
            fail({ errorText: "Can't update. Error: " + err });
          }
          else if (!result.modifiedCount) {
            fail({ errorText: "Nothing to update: requested id not found"});
          }
          else {
            done({ status: "Update succes" });
          }
        });
        db.close();
      })
      .catch((connectionError) => {
        if (connectionError) {
          fail({ errorText: "Can't connect to the MongoDB. Error: " + connectionError });
        }
      });
  });
};

let addContact = possibleContact => {
  return new Promise((done, fail) => {
    mongoClient.connect(mongoUrl)
      .then((db) => {
        let collection = db.collection('contacts');
        collection.insert([ possibleContact ], (err) => {
          if (err) {
            fail({ errorText: "Can't add entry. Error: " + err });
          }
          else {
            done({ status: "Contact added" });
          }
        });
        db.close();
      })
      .catch((connectionError) => {
        if (connectionError) {
          fail({ errorText: "Can't connect to the MongoDB. Error: " + connectionError });
        }
      });
  });
};

let deleteContact = contactIdToRemove => {
  return new Promise((done, fail) => {
    mongoClient.connect(mongoUrl)
      .then((db) => {
        let collection = db.collection('contacts');
        collection.deleteOne({ contactId: contactIdToRemove }, (err, result) => {
          if (err) {

            fail({ errorText: "Can't update contacts. Error: " + err });
          }
          else if (!result.deletedCount) {
            fail({ errorText: "Nothing to delete: requested id not found"});
          }
          else {
            done({ status: "Remove success" });
          }
        });
        db.close();
      })
      .catch((connectionError) => {
        if (connectionError) {
          fail({ errorText: "Can't connect to the MongoDB. Error: " + connectionError });
        }
      });
  });
};

// Read contacts
rtAPIv1.get("/contacts", function (req, res) {
  readContacts({})
    .then(contactsFound => { res.status(200).json(contactsFound); })
    .catch(errorMessage => { res.status(400).json(errorMessage); })
});

// New contact
rtAPIv1.post("/contacts", jsonParser, function(req, res) {
  checkOfData = checkUserInfo(req.body);
  if (checkOfData.passed) {
    let newName = String(req.body.name);
    let newSurname = String(req.body.surname);
    let newPhone = String(req.body.phone);
    let newId = getNewId();
    addContact({ name: newName, surname: newSurname, contactId: newId, phone: newPhone })
      .then(statusMessage => { res.status(200).json(statusMessage); })
      .catch( errorMessage => { res.status(400).json(errorMessage); });
  }
  else {
    res.status(400).json({info: checkOfData.info});
  }
});

// Update contact
rtAPIv1.put("/contacts/:id", jsonParser, function(req, res) {
  const idToFindRaw = req.params.id;
  if (!isValidUserId(idToFindRaw)) {
    res.status(400).json({info: "id must be an integer above zero"});
  } else {
    checkOfData = checkUserInfo(req.body);
    if (!checkOfData.passed) {
      res.status(400).json({info: checkOfData.info});
    }
    else {
      let newName = String(req.body.name);
      let newSurname = String(req.body.surname);
      let newPhone = String(req.body.phone);
      const idToFind = parseInt(idToFindRaw);

      updateContact(idToFind, { name: newName, surname: newSurname, phone: newPhone })
      .then(statusMessage => { res.status(200).json(statusMessage); })
      .catch( errorMessage => { res.status(400).json(errorMessage); });
    }
  }
});

// Remove contact
rtAPIv1.delete("/contacts/:id", function(req, res) {
  const idToFindRaw = req.params.id;
  if (!isValidUserId(idToFindRaw)) {
    res.status(400).json({info: "id must be an integer above zero"});
  } else {
    const idToFind = parseInt(idToFindRaw);
    deleteContact(idToFind)
      .then(statusMessage => { res.status(200).json(statusMessage); })
      .catch(() => { res.status(404).json({info: "your id not found"}); });
  }
});

// Search contact
rtAPIv1.post("/search", jsonParser, function(req, res) {
  if (!('name' in req.body) && !('surname' in req.body) && !('phone' in req.body)) {
    res.status(400).json({error: "At least one search field required: name, surname or phone"});
  }
  else {
    let searchForThis = {};
    if ('name' in req.body) {
      searchForThis['name'] = String(req.body.name);
    }
    if ('surname' in req.body) {
      searchForThis['surname'] = String(req.body.surname);
    }
    if ('phone' in req.body) {
      searchForThis['phone'] = String(req.body.phone);
    }
    readContacts(searchForThis)
      .then(contactsFound => { res.status(200).json(contactsFound); })
      .catch(errorMessage => { res.status(400).json(errorMessage); })
  }
});

addFirstUsers();
app.use("/api/v1", rtAPIv1);

app.listen(3000, function () { });
