#!/usr/bin/env bash

curl http://localhost:3000/api/v1/contacts -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/0 -i -X PUT -d '{"name":"Vasya","surname":"Likhachev","phone": "+7 (914) 321-32-32"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/777 -i -X PUT -d '{"name":"Vasya","surname":"Likhachev","phone": "+7 (914) 321-32-32"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/4 -i -X PUT -d '{"name":"Vasya","surname":"Likhachev"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/4 -i -X PUT -d '{"name":"Vasya","phone": "+7 (914) 321-32-32"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/4 -i -X PUT -d '{"surname":"Likhachev","phone": "+7 (914) 321-32-32"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts/4 -i -X PUT -d '{"name":"Boris","surname":"Tylenev","phone": "+7 (999) 333-22-11"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts -i -X GET
echo -e "\n";
