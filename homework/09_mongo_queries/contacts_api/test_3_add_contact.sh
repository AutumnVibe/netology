#!/usr/bin/env bash

curl http://localhost:3000/api/v1/contacts -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/contacts -i -X POST -d '{"name":"Vasya","surname":"Likhachev"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts -i -X POST -d '{"name":"Vasya","phone": "+7 (914) 321-32-32"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts -i -X POST -d '{"surname":"Likhachev","phone": "+7 (914) 321-32-32"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts -i -X POST -d '{"name":"Vasya","surname":"Likhachev","phone": "+7 (914) 321-32-32"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/contacts -i -X GET
echo -e "\n";
