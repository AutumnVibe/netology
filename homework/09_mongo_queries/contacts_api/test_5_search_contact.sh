#!/usr/bin/env bash

curl http://localhost:3000/api/v1/contacts -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/search -i -X POST -d '{}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/search -i -X POST -d '{"name":"Natalia"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/search -i -X POST -d '{"surname":"Shtein"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/search -i -X POST -d '{"name": "Alena", "surname":"Shtein"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/search -i -X POST -d '{"name": "Alena", "surname":"Petrova"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/search -i -X POST -d '{"phone": "+7 (925) 222-11-22"}' -H "Content-Type: application/json"
echo -e "\n";
