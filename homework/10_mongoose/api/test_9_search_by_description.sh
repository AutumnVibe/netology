#!/usr/bin/env bash

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Add Paris Streets", "description": "Add pictures of France capital", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Add London Streets", "description": "Add pictures of England capital", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/search_task_by_description -i -X POST -d '{"taskDescription":"capital"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/search_task_by_description -i -X POST -d '{"taskDescription":"capitalization"}' -H "Content-Type: application/json"
echo -e "\n";
