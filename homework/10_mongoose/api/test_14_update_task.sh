#!/usr/bin/env bash

curl http://localhost:3000/api/v1/users -i -X POST -d '{"name":"Lola"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Review", "description": "Lola must review all code", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/assign_task -i -X POST -d '{"taskName":"Review", "userName": "Lola"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/tasks/Review -i -X PUT -d '{"name":"Refactoring", "description": "Lola must refactor all code", "isOpen": "false"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";
