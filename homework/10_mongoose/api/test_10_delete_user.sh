#!/usr/bin/env bash

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users/Kolya -i -X DELETE
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X POST -d '{"name":"Kolya"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users/Kolya -i -X DELETE
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";
