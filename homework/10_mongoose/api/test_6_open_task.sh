#!/usr/bin/env bash

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Already closed task", "description": "I was created as closed", "isOpen": false}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/open_task -i -X POST -d '{"taskName":"Already closed task"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";
