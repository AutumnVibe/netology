#!/usr/bin/env bash

if [ -d "data" ]; then
    rm -rf data
fi

mkdir -p data/db

../mongodb/bin/mongod --dbpath data/db
