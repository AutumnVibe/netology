const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const express = require('express');
const app = express();
const rtAPIv1 = express.Router();

const mongoUrl = 'mongodb://localhost:27017/test_database';
mongoose.connect(mongoUrl);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));

function checkUserInfo(contentToCheck) {
  if (!('name' in contentToCheck)) {
    return { passed: 0, info: "name field required" };
  }
  return { passed: 1 };
}

function checkTaskInfo(contentToCheck) {
  if (!('name' in contentToCheck)) {
    return { passed: 0, info: "name field required" };
  }
  if (!('description' in contentToCheck)) {
    return { passed: 0, info: "description field required" };
  }
  if (!('isOpen' in contentToCheck)) {
    return { passed: 0, info: "isOpen field required" };
  }
  return { passed: 1 };
}

// User Schema
let userSchema = Schema({
  name: { type: String, unique: true },
});
let User = mongoose.model('User', userSchema);

// Task Schema
let taskSchema = Schema({
  name: { type: String, unique: true },
  description: String,
  isOpen: Boolean,
  assignedUser: {
    type: Schema.Types.ObjectId,
    ref: "User"
  }
});
let Task = mongoose.model('Task', taskSchema);

// Default test data
let ivan = new User({ name: 'Ivan'});
ivan.save();

let petr = new User({ name: 'Petr'});
petr.save();

let taskOne = new Task({ assignedUser: ivan, name: 'Fix servers', description: "Fix all servers after nodejs update", isOpen: false });
taskOne.save();

let taskTwo = new Task({ assignedUser: ivan, name: 'Update MongoDB', description: "Update MongoDB to latest version on servers before 2018", isOpen: true });
taskTwo.save();

let taskThree = new Task({ assignedUser: petr, name: 'Review code', description: "Review code from the outsource team", isOpen: true });
taskThree.save();

// API

// Read users
rtAPIv1.get("/users", function (req, res) {
  User.find(function (err) {
    if (err) {
      res.status(400).json({ errorMessage: err });
    }}).exec(function(error, found) {
      res.status(200).json({ usersFound: found })
    });
});

// Read tasks
rtAPIv1.get("/tasks", function (req, res) {
  Task.find(function (err) {
    if (err) {
      res.status(400).json({ errorMessage: err });
    }}).populate("assignedUser").exec(function(error, found) {
      res.status(200).json({ tasksFound: found })
    });
});

// New user
rtAPIv1.post("/users", jsonParser, function(req, res) {
  checkOfData = checkUserInfo(req.body);
  if (checkOfData.passed) {
    let newName = String(req.body.name);
    let newUser = new User({ name: newName });
    newUser.save()
      .then(() => { res.status(200).json({ "statusMessage": "Save success" }); })
      .catch(errorMessage => { res.status(400).json({ "errorMessage": errorMessage }); });
  }
  else {
    res.status(400).json({info: checkOfData.info});
  }
});

// New task
rtAPIv1.post("/tasks", jsonParser, function(req, res) {
  checkOfData = checkTaskInfo(req.body);
  if (checkOfData.passed) {
    let newName = String(req.body.name);
    let newDescription = String(req.body.description);
    let newIsOpen = Boolean(req.body.isOpen);
    let newTask = new Task({ name: newName, description: newDescription, isOpen: newIsOpen });
    newTask.save()
      .then(() => { res.status(200).json({ "statusMessage": "Save success" }); })
      .catch(errorMessage => { res.status(400).json({ "errorMessage": errorMessage }); });
  }
  else {
    res.status(400).json({info: checkOfData.info});
  }
});

// Assign task
rtAPIv1.post("/assign_task", jsonParser, function(req, res) {
  if (('taskName' in req.body) && ('userName' in req.body)) {
    let taskName = String(req.body.taskName);
    let userName = String(req.body.userName);
    Task.find({ name: taskName}, function (err) {
      if (err) {
        res.status(400).json({ errorMessage: err });
      }}).populate("assignedUser").exec(function(error, found) {
        if (found.length) {
          User.find({ name: userName}, function(errUserFind) {
            if (errUserFind) {
              res.status(400).json({ errorMessage: err });
            }
          }).exec(function(errorUserFind, userFound) {
            if (userFound.length) {
              Task.update({_id: found[0]['_id']}, { $set: { assignedUser: userFound[0]['_id']}}, function(errUpdate) {
                if (errUpdate) {
                  res.status(400).json({ errorMessage: errUpdate });
                }
                else {
                  res.status(200).json({ status: "Task assigned successfully" });
                }
              });
            }
            else {
              res.status(400).json({info: "User not found"});
            }
          });
        }
        else {
          res.status(400).json({info: "Task not found"});
        }
      });
  }
  else {
    res.status(400).json({info: "Requires task name and user name"});
  }
});

// Open task
rtAPIv1.post("/open_task", jsonParser, function(req, res) {
  if ('taskName' in req.body) {
    let taskName = String(req.body.taskName);
    Task.find({ name: taskName}, function (err) {
      if (err) {
        res.status(400).json({ errorMessage: err });
      }}).exec(function(error, found) {
        if (found.length) {
          Task.update({_id: found[0]['_id']}, { $set: { isOpen: true } }, function(errUpdate) {
            if (errUpdate) {
              res.status(400).json({ errorMessage: errUpdate });
            }
            else {
              res.status(200).json({ status: "Task opened successfully" });
            }
          });
        }
        else {
          res.status(400).json({info: "Task not found"});
        }
    });
  }
  else {
    res.status(400).json({info: "Task name required"});
  }
});

// Close task
rtAPIv1.post("/close_task", jsonParser, function(req, res) {
  if ('taskName' in req.body) {
    let taskName = String(req.body.taskName);
    Task.find({ name: taskName}, function (err) {
      if (err) {
        res.status(400).json({ errorMessage: err });
      }}).exec(function(error, found) {
        if (found.length) {
          Task.update({_id: found[0]['_id']}, { $set: { isOpen: false } }, function(errUpdate) {
            if (errUpdate) {
              res.status(400).json({ errorMessage: errUpdate });
            }
            else {
              res.status(200).json({ status: "Task closed successfully" });
            }
          });
        }
        else {
          res.status(400).json({info: "Task not found"});
        }
    });
  }
  else {
    res.status(400).json({info: "Task name required"});
  }
});

// Search task by name
rtAPIv1.post("/search_task_by_name", jsonParser, function(req, res) {
  if ('taskName' in req.body) {
    let taskName = String(req.body.taskName);
    Task.find({ name: new RegExp(taskName, "i")}, function (err) {
      if (err) {
        res.status(400).json({ errorMessage: err });
      }}).populate("assignedUser").exec(function(error, found) {
        res.status(200).json({ tasksFound: found })
    });
  }
});

// Search task by description
rtAPIv1.post("/search_task_by_description", jsonParser, function(req, res) {
  if ('taskDescription' in req.body) {
    let taskDescription = String(req.body.taskDescription);
    Task.find({ description: new RegExp(taskDescription, "i")}, function (err) {
      if (err) {
        res.status(400).json({ errorMessage: err });
      }}).populate("assignedUser").exec(function(error, found) {
        res.status(200).json({ tasksFound: found })
    });
  }
});

// Remove user
rtAPIv1.delete("/users/:name", function(req, res) {
  const nameToFindRaw = req.params.name;
  User.remove({ name: nameToFindRaw }, function(err, obj) {
    if (err) {
      res.status(400).json({"error": err});
    }
    else {
      if (obj.result.n === 0) {

        res.status(400).json({"error": "Nothing to remove"});
      }
      else {
        res.status(200).json({info: "Delete success"});
      }
    }
  });
});

// Remove task
rtAPIv1.delete("/tasks/:name", function(req, res) {
  const nameToFindRaw = req.params.name;
  Task.remove({ name: nameToFindRaw }, function(err, obj) {
    if (err) {
      res.status(400).json({"error": err});
    }
    else {
      if (obj.result.n === 0) {

        res.status(400).json({"error": "Nothing to remove"});
      }
      else {
        res.status(200).json({info: "Delete success"});
      }
    }
  });
});

// Update user
rtAPIv1.put("/users/:name", jsonParser, function(req, res) {
  checkOfData = checkUserInfo(req.body);
  if (checkOfData.passed) {
    let newName = String(req.body.name);
    const needName = req.params.name;

    User.find({ name: needName }, function(errUserFind) {
      if (errUserFind) {
        res.status(400).json({ errorMessage: err });
      }
    }).exec(function(errorUserFind, userFound) {
      if (userFound.length) {
        User.update({_id: userFound[0]['_id']}, { $set: { name: newName }}, function(errUpdate) {
          if (errUpdate) {
            res.status(400).json({ errorMessage: errUpdate });
          }
          else {
            res.status(200).json({ status: "Update success" });
          }
        });
      }
      else {
        res.status(400).json({info: "User not found"});
      }
    });
  }
  else {
    res.status(400).json({info: checkOfData.info});
  }
});

// Update task
rtAPIv1.put("/tasks/:name", jsonParser, function(req, res) {
  checkOfData = checkTaskInfo(req.body);
  if (checkOfData.passed) {
    let newName = String(req.body.name);
    let newDescription = String(req.body.description);
    let newIsOpen = Boolean(req.body.isOpen);
    const needName = req.params.name;

    Task.find({ name: needName }, function(errTaskFind) {
      if (errTaskFind) {
        res.status(400).json({ errorMessage: err });
      }
    }).exec(function(errorTaskFind, taskFound) {
      if (taskFound.length) {
        Task.update({_id: taskFound[0]['_id']}, { $set: { name: newName, isOpen: newIsOpen, description: newDescription }}, function(errUpdate) {
          if (errUpdate) {
            res.status(400).json({ errorMessage: errUpdate });
          }
          else {
            res.status(200).json({ status: "Update success" });
          }
        });
      }
      else {
        res.status(400).json({info: "Task not found"});
      }
    });
  }
  else {
    res.status(400).json({info: checkOfData.info});
  }
});

app.use("/api/v1", rtAPIv1);

app.listen(3000, function () { });
