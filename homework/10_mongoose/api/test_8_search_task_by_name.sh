#!/usr/bin/env bash

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Research Task One", "description": "Do it first", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Research Task Two", "description": "Do it second", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/search_task_by_name -i -X POST -d '{"taskName":"research"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/search_task_by_name -i -X POST -d '{"taskName":"advertisement"}' -H "Content-Type: application/json"
echo -e "\n";
