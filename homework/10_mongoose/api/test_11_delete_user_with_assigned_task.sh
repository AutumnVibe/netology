#!/usr/bin/env bash

curl http://localhost:3000/api/v1/users -i -X POST -d '{"name":"Tolya"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Strange", "description": "Strange task", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/assign_task -i -X POST -d '{"taskName":"Strange", "userName": "Tolya"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users/Tolya -i -X DELETE
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";