#!/usr/bin/env bash

curl http://localhost:3000/api/v1/users -i -X POST -d '{"name":"John"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Create report", "description": "Create report for publishers", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/assign_task -i -X POST -d '{"taskName":"Create report", "userName": "John"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";
