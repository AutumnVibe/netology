#!/usr/bin/env bash

curl http://localhost:3000/api/v1/users -i -X POST -d '{"name":"Carl"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Task for Carl", "description": "Simple task", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/assign_task -i -X POST -d '{"taskName":"Task for Carl", "userName": "Carl"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users/Carl -i -X PUT -d '{"name":"Kevin"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/users -i -X GET
echo -e "\n";
