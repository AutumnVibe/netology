#!/usr/bin/env bash

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Configure nginx"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X POST -d '{"name":"Configure nginx", "description": "Set configs for nginx proxies", "isOpen": true}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/api/v1/tasks -i -X GET
echo -e "\n";
