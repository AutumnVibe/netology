const expect = require('chai').expect;
const calculator = require("../src/calculator");

describe("Calculator", () => {

  // Task 1
  it('calculator is defined', () => {
    expect(calculator).is.not.undefined;
  });

  it('calculator has add', () => {
    expect(calculator.add).is.not.undefined;
  });

  it('calculator.add(",1") = 1', () => {
    expect(calculator.add(",1")).to.be.equal(1);
  });

  it('calculator.add(",1,2") = 3', () => {
    expect(calculator.add(",1,2")).to.be.equal(3);
  });

  it('calculator.add("") = 0 (default value)', () => {
    expect(calculator.add("")).to.be.equal(0);
  });

  // Task 2
  it('calculator.add(",1,2,3,4,5") = 15', () => {
    expect(calculator.add(",1,2,3,4,5")).to.be.equal(15);
  });

  // Task 3
  it('calculator.add("1\\n2,3") = 6', () => {
    expect(calculator.add("1\n2,3")).to.be.equal(6);
  });

  it('calculator.add("1,\\n") throws error', () => {
    expect(() => calculator.add("1,\n")).throws('not valid input');
  });

  it('calculator.add("1\\n,2") throws error', () => {
    expect(() => calculator.add("1\n,2")).throws('not valid input');
  });

  // Task 4
  it('calculator.add("//<;>\\n1;2;3;;4") throws error', () => {
    expect( calculator.add("//<;>\n1;2;3;;4")).to.be.equal(10);
  });

  // Task 5
  it('calculator.add("1,-2") throws error', () => {
    expect(() => calculator.add("1,-2")).throws('negative numbers are not allowed');
  });
})