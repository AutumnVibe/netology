exports.add = (inputStr) => {
  let separator = /[,\n]/
  if (/^\/\/<.>\n/.test(inputStr)) {
    separator = inputStr[3];
    inputStr = inputStr.slice(6,)
  }  
  else if (/(,\n)|(\n,)/.test(inputStr)) {
    throw new Error('not valid input');
  }

  const numbers = inputStr.split(separator);

  numbers.map(num => {
    if (parseInt(num) < 0) {
      throw new Error('negative numbers are not allowed');
    }
  });

  return numbers.reduce((result, number) => result + (parseInt(number) || 0), 0);
};
