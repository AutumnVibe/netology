const fs = require('fs');

let readDirWrapper = dirName => {
  return new Promise((resolve, reject) => {
    fs.readdir(dirName, (err, names) => {
      if (err) {
        reject(err);
      }
      resolve(names);
    })
  })
};

let readFileWrapper = (dirName, fileName) => {
  return new Promise((done, fail) => {
    fs.readFile(dirName + fileName, { 'encoding': 'utf8' }, (error, content) => {
      if (error) {
          fail(error);
       }
       done({ 'name': dirName + fileName, 'content': content });
    });
  })
};

let readAll = dirName => {
  return readDirWrapper(dirName)
    .then(names => Promise.all(names.map(fileName => readFileWrapper(dirName, fileName)) ))
};

module.exports = readAll;