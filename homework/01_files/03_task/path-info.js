const fs = require('fs');

let pathInfo = (path, callback) => {
  fs.stat(path, (err, stats) => {
    if (err) {
      callback({'error': err }, undefined);
    } else {
      if (stats.isFile()) {
        fs.readFile(path, { 'encoding': 'utf8' }, (error, content) => {
          if (error) {
            callback({'error': error }, undefined);
          } else {
            callback(null, { 'path': path, 'type': 'file', 'content': content, 'childs': undefined });
          }
        });
      } else if (stats.isDirectory()) {
        fs.readdir(path, (error, names) => {
          if (error) {
            callback({'error': error }, undefined);
          } else {
            callback(null, { 'path': path, 'type': 'directory', 'content': undefined, 'childs': names});
          }
        });
      } else {
        callback(null, undefined);
      }
    }
  });
};

module.exports = pathInfo;