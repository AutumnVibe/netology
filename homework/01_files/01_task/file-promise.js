const fs = require('fs');

let read = (file) => {
  return new Promise((done, fail) => {
    fs.readFile(file, { 'encoding': 'utf8' }, (err, content) => {
      if (err) {
        fail(err);
      }
      done(content);
    })
  });
};

let write = (file, data) => {
  return new Promise((done, fail) => {
    fs.writeFile(file, data, (err) => {
      if (err) {
        fail(err)
      }
      done(file);
    })
  });
};

module.exports = { read, write };