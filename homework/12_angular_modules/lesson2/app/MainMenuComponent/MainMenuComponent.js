'use strict';

angular
    .module('myApp')
    .component('mainMenuComponent', {
        templateUrl: 'MainMenuComponent/MainMenuComponent.html',
        controller: function() {
          let vm = this;
          vm.items = [
            { sref: "list", filler: "Список" },
            { sref: "createNewPokemon", filler: "Добавить нового" },
            { sref: "createNewUser", filler: "Добавить нового пользователя" }
          ];
        }
    })