const express = require('express');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const app = express();
const router = express.Router();

router.post('/post', jsonParser, function(req, res) {
  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res.status(404).send();
  }
  else {
    res.status(200).json(req.body);
  }
});

router.all('/sub/:first/:second?', function(req, res) {
  let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  res.status(200).send('"You requested URI: ' + fullUrl + '" ');
});

router.get('/hello/:name', function(req, res) {
  res.status(200).send('"Hello, ' + req.params.name + '!"');
});

router.get('/hello', function(req, res) {
  res.status(200).send('"Hello stranger!"');
});

router.get('/', function(req, res) {
  res.status(200).send('"Hello, Express.js"');
});

app.use('/', router);
app.listen(3000, function() {});
