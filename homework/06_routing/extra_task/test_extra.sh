#!/usr/bin/env bash

curl http://localhost:3000/post -i -X POST
echo -e "\n";

curl http://localhost:3000/post -i -X POST -H "Header: Key"
echo -e "\n";

curl http://localhost:3000/post -i -X POST -d '{"test":123,"qwe":"zxc"}' -H "Content-Type: application/json"
echo -e "\n";

curl http://localhost:3000/post -i -X POST -d '{"test":123,"qwe":"zxc"}' -H "Content-Type: application/json" -H "Header: Key"
echo -e "\n";
