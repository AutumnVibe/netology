const fs = require('fs');
const crypto = require('crypto');

const md5sum = crypto.createHash('md5');

let rstream = fs.createReadStream('file.txt');
let wstream = fs.createWriteStream('md5_of_file.txt');

rstream
  .pipe(md5sum)
  .on('data', (src) => { console.log('md5 of file is ' + src) })
  .pipe(wstream);
