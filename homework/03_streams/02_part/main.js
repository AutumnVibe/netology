const crypto = require('crypto');
const fs = require('fs');
const { Transform } = require('stream');

class CryptoTransform extends Transform {
  _transform(data, encoding, callback) {
    this.push(crypto.createHash('md5').update(data).digest('hex'));
    callback();
  }
}

let cryptoTransformStream = new CryptoTransform();
let readStream = fs.createReadStream('file.txt');
let writeStream = fs.createWriteStream('md5_hex_of_file.txt');

readStream
  .pipe(cryptoTransformStream)
  .on('data', (src) => { console.log('md5 hex of file is ' + src) })
  .pipe(writeStream);
