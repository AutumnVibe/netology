const { Readable, Writable, Transform } = require('stream');

class RandomReadable extends Readable {
  _read(size) {
      this.push((Math.floor(Math.random() * (10))).toString());
  }
}

class ConsoleWritable extends Writable {
  _write(chunk, encoding, callback) {
    console.log(chunk.toString());
    callback();
  }
}

class PeriodicTransform extends Transform {
  _transform(data, encoding, callback) {
    setTimeout(() => {
      this.push('modified_' + data.toString());
      callback();
    }, 1000);
  }
}

module.exports = { RandomReadable, ConsoleWritable, PeriodicTransform };

