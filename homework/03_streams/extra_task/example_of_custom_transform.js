const fs = require('fs');
const { PeriodicTransform } = require('./custom_streams');

let readStream = fs.createReadStream('file.txt');
let periodicTransformStream = new PeriodicTransform();

readStream
  .pipe(periodicTransformStream)
  .pipe(process.stdout);