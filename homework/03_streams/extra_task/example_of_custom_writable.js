const fs = require('fs');
const { ConsoleWritable } = require('./custom_streams');

let readStream = fs.createReadStream('file.txt');
let consoleWritableStream = new ConsoleWritable();

readStream
  .pipe(consoleWritableStream);