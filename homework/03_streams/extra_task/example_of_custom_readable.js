const { RandomReadable } = require('./custom_streams');

let randomReadableStream = new RandomReadable();

randomReadableStream
  .pipe(process.stdout);