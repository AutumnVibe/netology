const { Pokemon, Pokemonlist } = require('../pokemon');

const assert = require('assert');
const expect = require('chai').expect;
const sinon  = require('sinon');

let spy = sinon.spy(console, 'log');

describe('Pokemon', () => {
  const pokemon = new Pokemon("Pikachu", 80);

  it('show must print string to console.log', () => {
    pokemon.show();
    assert(spy.calledWith('Hi! My name is Pikachu, my level is 80'), 'show must be: Hi! My name is Pikachu, my level is 80');
  });

  it('valueOf must show correct level', () => {
    assert(80 === pokemon.valueOf(), 'valueOf must return 80');
  });
});

describe('Pokemonlist', () => {
  const pokemonlist = new Pokemonlist();

  it('add must add pokemon to pokemonlist and show must show it', () => {
    pokemonlist.add("Pikachu", 20);
    pokemonlist.show();
    assert(spy.calledWith('There are 1 pokemons here.'), 'show must be: There are 1 pokemons here.');
  });

  it('max must show strongest pokemon', () => {
    pokemonlist.add("Pidgeot", 10);
    let strongestPokemon = pokemonlist.max();
    assert(20 == strongestPokemon.level, 'strongest pokemon level must be 20');
  });
});
