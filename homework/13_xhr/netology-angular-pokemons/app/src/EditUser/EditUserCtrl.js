'use strict'

userApp.controller('EditUserCtrl', function ($scope, $routeParams, UsersService) {
  UsersService.getUser($routeParams['userId']).then(function (response) {
    $scope.editThisUser = response.data
    $scope.userLoaded = true
  })

  $scope.editUser = function (myUser) {
    $scope.editSuccess = false

    UsersService.editUser(myUser).then(function (response) {
      $scope.editUser = {}

      $scope.editUserId = response.data.id
      $scope.editSuccess = true
    })
  }
})
