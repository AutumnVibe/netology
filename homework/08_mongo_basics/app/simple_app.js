const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;
const mongoUrl = 'mongodb://localhost:27017/test_database';

function addUsers(callback) {
  mongoClient.connect(mongoUrl, (err, db) => {
    if (err) {
      console.log("Can't connect to the MongoDB. Error: ", err);
    }
    else {
      let collection = db.collection('users');

      let testUsers = [
        { name: "Ivan", surname: "Smirnov" },
        { name: "Anna", surname: "Andreeva" },
        { name: "Maria", surname: "Petrova" },
        { name: "Daria", surname: "Kuznetsova" },
        { name: "Anna", surname: "Ivanova" },
        { name: "Oleg", surname: "Grigoriev" },
        { name: "Anna", surname: "Aleksandrova" },
      ];

      collection.insert(testUsers, (err) => {
        if (err) {
          console.log("Can't insert test users. Error: ", err);
        }
        else {
          showUsers();
          updateUsers();
        }
      });
      db.close();
    }
  });
};

function showUsers() {
  mongoClient.connect(mongoUrl, (err, db) => {
    if (err) {
      console.log("Can't connect to the MongoDB. Error: ", err);
    }
    else {
      let collection = db.collection('users');

      collection.find().toArray((err, result) => {
        if (err) {
          console.log("Can't show test users. Error: ", err)
        }
        else if (result.length) {
          console.log("Found users:\n", result);
        }
        else {
          console.log("Users not found");
        }
      });
      db.close();
    }
  });
};

function updateUsers() {
  mongoClient.connect(mongoUrl, (err, db) => {
    if (err) {
      console.log("Can't connect to the MongoDB. Error: ", err);
    }
    else {
      let collection = db.collection('users');

      collection.updateMany({ "name": "Anna" }, { "$set": { "name": "Aleksandra" } }, (err) => {
        if (err) {
          console.log("Can't update users. Error: ", err)
        }
        else {
          console.log("\n\n===== Update success =====\n\n");
          showUsers();
          removeUsers();
        }
      });
      db.close();
    }
  });
};

function removeUsers() {
  mongoClient.connect(mongoUrl, (err, db) => {
    if (err) {
      console.log("Can't connect to the MongoDB. Error: ", err);
    }
    else {
      let collection = db.collection('users');

      collection.remove({ "name": "Aleksandra" }, (err) => {
        if (err) {
          console.log("Can't update users. Error: ", err)
        }
        else {
          console.log("\n\n===== Remove success =====\n\n");
          showUsers();
        }
      });
      db.close();
    }
  });
}

addUsers();
