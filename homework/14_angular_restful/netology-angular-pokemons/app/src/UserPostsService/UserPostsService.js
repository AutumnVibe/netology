angular
  .module('UserApp')
  .factory('UserPostsService', function ($resource, $http) {
    return $resource('https://jsonplaceholder.typicode.com/users/:userId/posts')
  })
